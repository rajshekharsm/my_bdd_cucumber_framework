Feature: Trigger the Patch API 
@Data_Driven_Patch
Scenario Outline: Trigger the Patch api request request body parameters
    Given Enter "<Name>" and "<Job>" in request body for Patch 
	When send the patch request with payload with EndPoint 
	Then Validate the Status code for Patch API 
	And Validate the responsebody parameters for Patch API 
	
Examples: 
		|Name|Job|
		|Raj|Sr.Tester|
		|Swaraj|BA|
		|Sachin|Tester|