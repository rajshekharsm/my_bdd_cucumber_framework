Feature: Trigger the API On the basis of Input Data
@Data_Driven
Scenario Outline: Trigger Post api request 
	Given "<Name>" and <"Job"> in request body
	When send the request with payload with EndPoint 
	Then Validate the Status code 
	And Validate the responsebody parameters

Examples: 
			|Name|Job|
			|Raj|QA|
			|Swaraj|SM|