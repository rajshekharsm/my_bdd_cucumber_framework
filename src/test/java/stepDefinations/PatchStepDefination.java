package stepDefinations;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Request_Body;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PatchStepDefination {
	String RequestBody;
	String Endpoint;
	Response response;
	int statuscode;
	String res_body;
	String res_name;
	String res_job;
	String res_updatedAt;

@Given("Enter {string} and {string} in request body for Patch")
public void Enter_in_request_body_for_Patch(String req_name, String req_job) throws IOException
{
	File Dir_name = Utility.CreateLogDirectory("Patch_Api_Log");
	RequestBody ="{\r\n" + "    \"name\": \""+req_name+"\",\r\n" + "    \"job\": \""+req_job+"\"\r\n" + "}";

	Endpoint = Request_Body.Hostname() + Request_Body.Resource_Patch_TasteCase();

	response = API_Trigger.Patch_trigger(Request_Body.Headername(), Request_Body.Headervalue(),
			Request_Body.Req_Patch_TestCase(), Endpoint);

	Utility.evidenceFileCreator(Utility.testLogName("Patch_log"), Dir_name, Endpoint,
			Request_Body.Req_Patch_TestCase(), response.getHeader("Date"), response.getBody().asPrettyString());

	
}
	
	
	
	@Given("Enter NAME and JOB in request body for Patch API")
public void Enter_NAME_and_JOB_in_request_body_for_Patch_API() throws IOException
{
	File Dir_name = Utility.CreateLogDirectory("Patch_Api_Log");
	RequestBody = Request_Body.Req_Patch_TestCase();

	Endpoint = Request_Body.Hostname() + Request_Body.Resource_Patch_TasteCase();

	response = API_Trigger.Patch_trigger(Request_Body.Headername(), Request_Body.Headervalue(),
			Request_Body.Req_Patch_TestCase(), Endpoint);

	Utility.evidenceFileCreator(Utility.testLogName("Patch_log"), Dir_name, Endpoint,
			Request_Body.Req_Patch_TestCase(), response.getHeader("Date"), response.getBody().asPrettyString());

	//throw new io.cucumber.java.PendingException();
}
@When("send the patch request with payload with EndPoint")
public void Send_the_request_with_payload_For_Patch_API()
{
	statuscode = (response.statusCode());
	res_body = response.getBody().asString();
	res_name = response.jsonPath().getString("name");
	res_job = response.jsonPath().getString("job");
	res_updatedAt = response.getBody().jsonPath().getString("updatedAt");
	res_updatedAt = res_updatedAt.substring(0, 11);
	// throw new io.cucumber.java.PendingException();

}
@Then("Validate the Status code for Patch API")
public void Validate_status_code_of_Patch_API()
{
	Assert.assertEquals(statuscode, 200);
	// throw new io.cucumber.java.PendingException();
	
}	
@Then("Validate the responsebody parameters for Patch API")
public void Validate_response_body_parameters_of_Patch_API()
{
	JsonPath jsp_req = new JsonPath(Request_Body.Req_Patch_TestCase());
	String req_name = jsp_req.getString("name");
	String req_job = jsp_req.getString("job");
	LocalDateTime currentdate = LocalDateTime.now();
	String expecteddate = currentdate.toString().substring(0, 11);
	Assert.assertEquals(res_name, req_name);
	Assert.assertEquals(res_job, req_job);
	Assert.assertEquals(res_updatedAt.substring(0, 11), expecteddate);
	// throw new io.cucumber.java.PendingException();
}

}
