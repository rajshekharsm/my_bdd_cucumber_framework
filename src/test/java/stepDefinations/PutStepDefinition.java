package stepDefinations;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Request_Body;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class PutStepDefinition {
	String requestbody;
	String Endpoint;
	Response response;
	String res_body;
	int statuscode;
	String res_name;
	String res_job;
	String res_updatedAt;
	String requestBody;
	@Given("{string} and {string} in request body for Put")
	public void Enter_Name_Job_in_request_body_for_Put(String req_name, String req_job) throws IOException
	{
		File dir_name = Utility.CreateLogDirectory("Put_Log");
		requestBody = "{\r\n" + "    \"Name\": \""+req_name+"\",\r\n" + "    \"Job\": \""+req_job+"\"\r\n" + "}";
		Endpoint = Request_Body.Hostname() + Request_Body.Resource_Put_TestCase();
		response = API_Trigger.PUT_Trigger(Request_Body.Headername(), Request_Body.Headervalue(),
				Request_Body.Req_Put_TestCase(), Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("Put_Log"), dir_name, Endpoint, Request_Body.Req_Put_TestCase(),
				response.getHeader("Date"), response.getBody().asString());
		
	}
	
	
	@Given("Enter Name and Job in request body for Put")
	public void enterNameAndJobInRequestBodyForPut() throws IOException {
		File dir_name = Utility.CreateLogDirectory("Put_Log");
		requestbody = Request_Body.Req_Put_TestCase();
		Endpoint = Request_Body.Hostname() + Request_Body.Resource_Put_TestCase();
		response = API_Trigger.PUT_Trigger(Request_Body.Headername(), Request_Body.Headervalue(),
				Request_Body.Req_Put_TestCase(), Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("Put_Log"), dir_name, Endpoint, Request_Body.Req_Put_TestCase(),
				response.getHeader("Date"), response.getBody().asString());
		// throw new io.cucumber.java.PendingException();

	}

	@When("send the put request with payload with EndPoint")
	public void sendPutRequestWithPayloadWithEndPoint() {
		res_body = (response.getBody().asString());
		statuscode = (response.statusCode());
		res_name = (response.jsonPath().getString("name"));
		res_job = (response.jsonPath().getString("job"));
		res_updatedAt = response.jsonPath().getString("updatedAt").substring(0, 11);
		// throw new io.cucumber.java.PendingException();

	}

	@Then("Validate the Status code for PUT API")
	public void validateStatusCodeForPutApi() {
		Assert.assertEquals(statuscode, 200);
		// throw new io.cucumber.java.PendingException();

	}

	@Then("Validate the responsebody parameters for PUT API")
	public void validateResponseBodyParametersForPutApi() {
		JsonPath jsp_req = new JsonPath(Request_Body.Req_Put_TestCase());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		Assert.assertEquals(response.jsonPath().getString("name"), req_name);
		Assert.assertEquals(response.jsonPath().getString("job"), req_job);
		Assert.assertEquals(response.jsonPath().getString("updatedAt").substring(0, 11), expecteddate);
		// throw new io.cucumber.java.PendingException();

	}
}
