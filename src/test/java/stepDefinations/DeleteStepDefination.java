package stepDefinations;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Request_Body;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;

public class DeleteStepDefination {
	File dir_name;
	Response response;
	int status_code;
	@Given("Delete API")
	public void Delete_API() throws IOException {
		String Endpoint = Request_Body.Hostname() + Request_Body.Resource_Delect_Testcase();

		dir_name = Utility.CreateLogDirectory("Delete_Api_Log");
		response = API_Trigger.Delete_Trigger(Request_Body.Hostname(), Request_Body.Headervalue(), Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("Delete_Log"), dir_name, Endpoint, null,
				response.getHeader("Date"), null);
		// throw new io.cucumber.java.PendingException();
	}
	@When("Send the delete API with Endpoint")
	public void Send_the_delete_API_with_Endpoint()
	{
		status_code = response.statusCode();
		
		// throw new io.cucumber.java.PendingException();
	}
	@Then ("Validate the Status code for DELETE API")
	public void Validate_the_Status_code_for_DELETE_API()
	{
		Assert.assertEquals(status_code, 204);
	}
}
