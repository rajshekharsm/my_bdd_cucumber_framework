package stepDefinations;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Request_Body;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class poststepdefination {
	Response response;
	int statuscode;
	String res_name;
	String res_job;
	String res_id;
	String res_createdAt;
	String requestBody;
	ResponseBody res_body;
	String hostname;
	String resource;
	String headername;
	String headervalue;
	String Endpoint;

	@Given("{string} and <{string}> in request body")
	public void and_in_request_body(String req_name, String req_job) throws IOException {
		File dir_Name = Utility.CreateLogDirectory("Api_Log");
		requestBody = "{\r\n" + "    \"Name\": \""+req_name+"\",\r\n" + "    \"Job\": \""+req_job+"\"\r\n" + "}";
		String Endpoint = Request_Body.Hostname() + Request_Body.Resource();
		response = API_Trigger.Post_trigger(Request_Body.Headername(), Request_Body.Headervalue(), requestBody,
				Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("Data_Driven_Test"), dir_Name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
		
	}



	@Given("Enter Name and Job in request body")
	public void enter_name_and_job_in_request_body() throws IOException {
		File dir_Name = Utility.CreateLogDirectory("Api_Log");
		requestBody = Request_Body.Req_Tc1();
		String Endpoint = Request_Body.Hostname() + Request_Body.Resource();
		response = API_Trigger.Post_trigger(Request_Body.Headername(), Request_Body.Headervalue(), requestBody,
				Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("Post_Test"), dir_Name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());

	}

	@When("send the request with payload with EndPoint")
	public void send_the_request_with_payload_with_end_point() {
		statuscode = response.statusCode();
		ResponseBody res_body = response.getBody();
		res_name = (response.jsonPath().getString("name"));
		res_job = (response.jsonPath().getString("job"));
		res_id = (response.jsonPath().getString("id"));
		res_createdAt = response.jsonPath().getString("createdAt").substring(0, 11);
		// throw new io.cucumber.java.PendingException();

	}

	@Then("Validate the Status code")
	public void validate_the_status_code() {
		Assert.assertEquals(statuscode, 201);
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate the responsebody parameters")
	public void validate_the_responsebody_parameters() {

		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_createdAt, expecteddate);
		Assert.assertNotNull(res_id);
	}
	// throw new io.cucumber.java.PendingException();
}
