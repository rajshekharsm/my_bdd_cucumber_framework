package Common_Methods;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class TestNGRetryAnalayzer  implements IRetryAnalyzer{
	private int countstart = 0;
	private int countend = 4;

	@Override
	public boolean retry(ITestResult result) {
		if (countstart < countend) {
			String testcasename = result.getName();
			System.out.println(testcasename + " failed in iteration " + countstart + ", Hence retrying "
					+ (countstart + 1));
			countstart ++;
			return true;
		}

		return false;
	}


}
