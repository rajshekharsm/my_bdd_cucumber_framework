package Common_Methods;

import Repository.Request_Body;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class API_Trigger {
	public static Response Post_trigger(String Headername, String Headervalue, String Req_Body, String Endpoint) {

		RequestSpecification req_spec = RestAssured.given();

		req_spec.header(Headername, Headervalue);

		req_spec.body(Req_Body);

		Response response = req_spec.post(Endpoint);

		return response;
	}

	public static Response Patch_trigger(String Headername, String Headervalue, String Req_Body, String Endpoint) {

		RequestSpecification req_spec = RestAssured.given();

		req_spec.header(Headername, Headervalue);

		req_spec.body(Req_Body);

		Response response = req_spec.patch(Endpoint);

		return response;
	}

	public static Response PUT_Trigger(String Headername, String Headervalue, String Req_Body, String Endpoint) {

		RequestSpecification req_spec = RestAssured.given();

		req_spec.header(Headername, Headervalue);

		req_spec.body(Req_Body);

		Response response = req_spec.put(Endpoint);

		return response;
	}

	public static Response Get_Trigger(String Headername, String Headervalue, String Endpoint) {

		RequestSpecification req_spec = RestAssured.given();

		req_spec.header(Headername, Headervalue);

		Response response = req_spec.get(Endpoint);

		return response;
	}

	public static Response Delete_Trigger(String Headername, String Headervalue, String Endpoint) {

		RequestSpecification req_spec = RestAssured.given();

		req_spec.header(Headername, Headervalue);

		Response response = req_spec.delete(Endpoint);

		return response;
	}

}
