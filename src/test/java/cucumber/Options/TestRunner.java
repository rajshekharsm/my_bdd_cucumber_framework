package cucumber.Options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith (Cucumber.class)
@CucumberOptions(features = "src/test/java/features",glue= {"stepDefinations"},tags="@Data_Driven or @post_api or @Data_Driven_Put or @Data_Driven_Patch")

public class TestRunner {

}
