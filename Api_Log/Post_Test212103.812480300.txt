Endpoint is :
https://reqres.in/api/users

Request body is :
{
    " name": "morpheus",
    "job": "leader"
}

Response header date is : 
Sat, 16 Mar 2024 15:51:03 GMT

Response body is : 
{" name":"morpheus","job":"leader","id":"499","createdAt":"2024-03-16T15:51:03.740Z"}