Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Fri, 15 Mar 2024 17:22:51 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"262","createdAt":"2024-03-15T17:22:51.012Z"}