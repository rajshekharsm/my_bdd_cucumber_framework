Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Fri, 15 Mar 2024 12:11:51 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"974","createdAt":"2024-03-15T12:11:50.997Z"}