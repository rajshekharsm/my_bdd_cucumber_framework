Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Fri, 15 Mar 2024 13:40:32 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"751","createdAt":"2024-03-15T13:40:32.258Z"}