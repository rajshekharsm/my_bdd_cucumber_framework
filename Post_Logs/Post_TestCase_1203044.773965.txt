Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Fri, 15 Mar 2024 15:00:46 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"216","createdAt":"2024-03-15T15:00:46.051Z"}