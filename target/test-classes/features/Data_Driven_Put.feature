Feature: Data Driven the Put API 
@Data_Driven_Put 
Scenario Outline: Trigger the Put api request request body parameters 
	Given "<Name>" and "<Job>" in request body for Put 
	When send the put request with payload with EndPoint 
	Then Validate the Status code for PUT API 
	And Validate the responsebody parameters for PUT API 
	
Examples: 
		|Name|Job|
		|Raj|Sr.Tester|
		|Swaraj|BA|
		|Sachin|Tester|