Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "morpheus",
    "job": "zion resident"
}

Response header date is : 
Sat, 16 Mar 2024 03:02:19 GMT

Response body is : 
{"name":"morpheus","job":"zion resident","updatedAt":"2024-03-16T03:02:19.815Z"}