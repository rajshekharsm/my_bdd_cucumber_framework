Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "morpheus",
    "job": "zion resident"
}

Response header date is : 
Sat, 16 Mar 2024 03:03:46 GMT

Response body is : 
{"name":"morpheus","job":"zion resident","updatedAt":"2024-03-16T03:03:46.236Z"}